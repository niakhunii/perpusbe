<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//register
$router->post('perpus/user', 'UserController@register' );
//login
$router->post('perpus/login', 'UserController@login' );
//getuser
$router->get('perpus/get_user', 'UserController@getUserall' );
//updateuser
$router->put('perpus/update_user/{id_user}', 'UserController@updateUser' );
//getuserbyid
$router->get('perpus/get_user_by_id/{id_user}', 'UserController@getuserID' );




//insertbuku
$router->post('perpus/insert_buku', 'BukuController@data_buku' );
//getbuku
$router->get('perpus/get_buku', 'BukuController@getBuku' );
//updatebuku
$router->put('perpus/update_buku/{kd_buku}', 'BukuController@updateBuku' );
//deletebuku
$router->delete('perpus/delete_buku/{kd_buku}', 'BukuController@deleteBuku' );
//getbukubyid
$router->get('perpus/get_buku_by_id/{kd_buku}', 'BukuController@getBukuID' );


//insertPinjaman
$router->post('perpus/insert_pinjam', 'PinjamanController@data_pinjaman' );
//getPinjaman
$router->get('perpus/get_pinjam', 'PinjamanController@getPinjaman' );
//updatePinjaman
$router->put('perpus/update_pinjam/{kd_pinjam}', 'PinjamanController@updatePinjaman' );
//deletePinjaman
$router->delete('perpus/delete_pinjam/{kd_pinjam}', 'PinjamanController@deletePinjaman' );

