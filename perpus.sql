-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for perpus
CREATE DATABASE IF NOT EXISTS `perpus` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `perpus`;

-- Dumping structure for table perpus.data_buku
CREATE TABLE IF NOT EXISTS `data_buku` (
  `kd_buku` int(50) NOT NULL AUTO_INCREMENT,
  `nama_buku` varchar(250) DEFAULT NULL,
  `penerbit` varchar(50) DEFAULT NULL,
  `tgl_terbit` date DEFAULT NULL,
  `jmlh_buku` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_buku`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table perpus.data_buku: ~2 rows (approximately)
/*!40000 ALTER TABLE `data_buku` DISABLE KEYS */;
INSERT INTO `data_buku` (`kd_buku`, `nama_buku`, `penerbit`, `tgl_terbit`, `jmlh_buku`) VALUES
	(1, 'join buku', 'hanys', '2021-09-23', 5),
	(5, 'julian and robbert', 'Elky', '2021-09-24', 12);
/*!40000 ALTER TABLE `data_buku` ENABLE KEYS */;

-- Dumping structure for table perpus.data_pinjam
CREATE TABLE IF NOT EXISTS `data_pinjam` (
  `kd_pinjam` int(50) NOT NULL AUTO_INCREMENT,
  `kd_buku` varchar(50) DEFAULT NULL,
  `tgl_pinjam` date DEFAULT NULL,
  `id_user_pinjam` varchar(50) DEFAULT NULL,
  `tgl_pengembalian` date DEFAULT NULL,
  PRIMARY KEY (`kd_pinjam`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table perpus.data_pinjam: ~2 rows (approximately)
/*!40000 ALTER TABLE `data_pinjam` DISABLE KEYS */;
INSERT INTO `data_pinjam` (`kd_pinjam`, `kd_buku`, `tgl_pinjam`, `id_user_pinjam`, `tgl_pengembalian`) VALUES
	(5, '5', '2021-09-24', '2', '2021-09-24'),
	(6, '5', '2021-09-24', '1', '2021-09-24');
/*!40000 ALTER TABLE `data_pinjam` ENABLE KEYS */;

-- Dumping structure for table perpus.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `aktif` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_user`) USING BTREE,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table perpus.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id_user`, `username`, `password`, `role`, `nama`, `tgl_lahir`, `no_telp`, `alamat`, `aktif`) VALUES
	('1', 'User22', '29b19605f242f18265db619d5f4edf13', '22', 'User22', '2021-09-24', '2637490', 'Depok', '1'),
	('2', 'khuniati@tes.com', '098f6bcd4621d373cade4e832627b4f6', '1', 'Khuniati', '2021-09-24', '089601230489', 'Depok', '1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
