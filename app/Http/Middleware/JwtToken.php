<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\Models\ModelUser;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use DB;
class JwtMiddleware {
    public function handle($request, Closure $next, $guard = null) {
        $token = $request->header('appsession');
        $key = "secret";
        
        if(!$token) {
            return response()->json([
            'status' => '0',
            'error' => 'Token not provided.'
            ], 200);
        }

        try {
            $credentials = JWT::decode($token, $key, ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
            'status' => '0',
            'error' => 'Provided token is expired.'
            ], 200);
        } catch(Exception $e) {
            
            return response()->json([
            'status' => '0',
            'error' => 'An error token.',
            ], 200);
        }

        $data = DB::table('user')
        ->select('*')
        ->where('id', $credentials->id)
        ->get();

        if(!$data) {
            return response()->json([
                'status' => '0',
                'error' => 'Data not found.'
                ], 200);
        }

        $request->auth = $data;
        return $next($request);
    }
}