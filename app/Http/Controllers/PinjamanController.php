<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DB;

class PinjamanController 
{
    //

    public function data_pinjaman ( Request $request ) {

        $kd_pinjam = $request->input('kd_pinjam');
        $kd_buku = $request->input('kd_buku');
        $tgl_pinjam = $request->input('tgl_pinjam');
        $id_user_pinjam = $request->input('id_user_pinjam');
        $tgl_pengembalian = $request->input('tgl_pengembalian');
        $tgl_pinjam = date('Y-m-d');
        $tgl_pengembalian = date('Y-m-d');

        $cekdatapinjaman = DB::table('data_pinjam')
        ->select('*')
        ->where('kd_pinjam', $kd_pinjam)
        ->count();

        if ( $cekdatapinjaman > 0 ) {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Data Buku sudah ada',
                'message_en' => 'Book data already exists',
            ], 401);
        }

        $data = DB::table('data_pinjam')
        ->insert(
            [
                'kd_buku' => $kd_buku,
                'tgl_pinjam' => $tgl_pinjam,
                'id_user_pinjam' => $id_user_pinjam,
                'tgl_pengembalian' => $tgl_pengembalian,
            ]
            
        );

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Menyimpan',
                'message_en' => 'Success Save',
                'data' => $data,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Menyimpan',
                'message_en' => 'Error Save',
                'data' => $data,
            ], 401);
        }
        
    }


    function getPinjaman () {

        try {
            $data = DB::table('data_pinjam')
            ->select('*')
            ->join('user','data_pinjam.id_user_pinjam', '=', 'user.id_user')
            ->join('data_buku','data_pinjam.kd_buku', '=', 'data_buku.kd_buku')
            ->get();
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $data
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }


    public function updatePinjaman( Request $request, $kd_pinjam ) {
        $tgl_pengembalian= $request->input('tgl_pengembalian');


        $data = DB::table('data_pinjam')
        ->where('kd_pinjam', $kd_pinjam)
        ->update(
            [
                "tgl_pengembalian"=> $tgl_pengembalian
            ]
        );

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Ubah Data Buku',
                'message_en' => 'Success Update Data Book',
                'data' => $kd_pinjam,
                'data1' => $tgl_pengembalian,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Masukan Data Buku',
                'message_en' => 'Error Input Data Book',
                'data' => $kd_pinjam,
                'data1' => $tgl_pengembalian,
            ], 401);
        }
    }


    function deletePinjaman($kd_pinjam){
        try {
            $data = DB::table('data_pinjam')
            ->where('kd_pinjam','=', $kd_pinjam)
            ->delete();
            if(!$data){
                return response()->json([
                    'status' => 0,
                    'message' => "error"
                ], 200);
            }
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $kd_pinjam
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

    function Joinpinjaman($kd_pinjam){
        try {
            $data = DB::table('data_pinjam')
            ->where('kd_pinjam','=', $kd_pinjam)
            ->delete();
            if(!$data){
                return response()->json([
                    'status' => 0,
                    'message' => "error"
                ], 200);
            }
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $kd_pinjam
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

}


