<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use DB;

class BukuController 
{
    //

    public function data_buku ( Request $request ) {

        $kd_buku = $request->input('kd_buku');
        $nama_buku = $request->input('nama_buku');
        $penerbit = $request->input('penerbit');
        $tgl_terbit = $request->input('tgl_terbit');
        $jmlh_buku = $request->input('jmlh_buku');
        $tgl_terbit = date('Y-m-d');

        $cekdatabuku = DB::table('data_buku')
        ->select('*')
        ->where('nama_buku', $nama_buku)
        ->count();

        if ( $cekdatabuku > 0 ) {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Data Buku sudah ada',
                'message_en' => 'Book data already exists',
            ], 401);
        }

        $data = DB::table('data_buku')
        ->insert(
            [
                'nama_buku' => $nama_buku,
                'penerbit' => $penerbit,
                'tgl_terbit' => $tgl_terbit,
                'jmlh_buku' => $jmlh_buku,
            ]
        );

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Menyimpan',
                'message_en' => 'Success Save',
                'data' => $data,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Menyimpan',
                'message_en' => 'Error Save',
                'data' => $data,
            ], 401);
        }
    }

    function getBuku () {

        try {
            $data = DB::table('data_buku')
            ->select('*') 
            ->get();
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $data
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

    function getBukuID ($kd_buku) {
        try {
            $data = DB::table('data_buku')
            ->select('*') 
            ->where('kd_buku', $kd_buku)
            ->get();
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $data
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }




    public function updateBuku( Request $request, $kd_buku ) {
        $nama_buku= $request->input('nama_buku');
        $penerbit= $request->input('penerbit');
        $tgl_terbit = $request->input('tgl_terbit');
        $jmlh_buku = $request->input('jmlh_buku');
        
       
        $data = DB::table('data_buku')
        ->where('kd_buku', $kd_buku)
        ->update(
            [
                "nama_buku"=> $nama_buku,
                "penerbit"=> $penerbit,
                "tgl_terbit" => $tgl_terbit,
                "jmlh_buku" => $jmlh_buku
            ]
        );

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Ubah Data Buku',
                'message_en' => 'Success Update Data Book',
                'data' => $kd_buku,
                'nama_buku' => $nama_buku,
                'penerbit' => $penerbit,
                'tgl_terbit' => $tgl_terbit,
                'jmlh_buku' => $jmlh_buku,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Masukan Data Buku',
                'message_en' => 'Error Input Data Book',
                'data' => $kd_buku,
                'nama_buku' => $nama_buku,
                'penerbit' => $penerbit,
                'tgl_terbit' => $tgl_terbit,
                'jmlh_buku' => $jmlh_buku,
            ], 401);
        }
    }


    function deleteBuku($kd_buku){
        try {
            $data = DB::table('data_buku')
            ->where('kd_buku','=', $kd_buku)
            ->delete();
            if(!$data){
                return response()->json([
                    'status' => 0,
                    'message' => "error"
                ], 200);
            }
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $kd_buku
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

}


