<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MailController;
use DB;
use Firebase\JWT\ExpiredException;

class UserController 
{
    //
    
    public function register ( Request $request ) {

        $username = $request->input('username');
        $password = $request->input('password');
        $role = $request->input('role');
        $nama = $request->input('nama');
        $tgl_lahir = $request->input('tgl_lahir');
        $no_telp = $request->input('no_telp');
        $alamat = $request->input('alamat');
        $aktif = $request->input('aktif');
        $password = md5($password);
        $tgl_lahir = date('Y-m-d');

        $cekMail = DB::table('user')
        ->select('*')
        ->where('username', $username)
        ->count();

        if ( $cekMail > 0 ) {
            return response()->json([
                'status' => 0,
                'message_ind' => 'username Anda Sudah Digunakan',
                'message_en' => 'You username Already use',
            ], 401);
        }

        $data = DB::table('user')
        ->insert(
            [
                'username' => $username,
                'password' => $password,
                'role' => $role,
                'nama' => $nama,
                'tgl_lahir' => $tgl_lahir,
                'no_telp' => $no_telp,
                'alamat' => $alamat,
                'aktif' => $aktif,
            ]
        );

        

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Registrasi',
                'message_en' => 'Success Registration',
                'data' => $data,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Registrasi',
                'message_en' => 'Error Registartion',
                'data' => $data,
            ], 401);
        }
    }

    function getUserall() {

        try {
            $data = DB::table('user')
            ->select('*')
            ->get();
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $data
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

    function getuserID ($id_user) {
        try {
            $data = DB::table('user')
            ->select('*') 
            ->where('id_user', $id_user)
            ->get();
            return response()->json([
                'status' => 1,
                'message' => "success",
                'data' => $data
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 0,
                'message' => $exception->getMessage(),
            ], 401);
        }
    }

    public function updateUser( Request $request, $id_user ) {
        $username= $request->input('username');
        $role = $request->input('role');
        $nama = $request->input('nama');
        $tgl_lahir= $request->input('tgl_lahir');
        $no_telp = $request->input('no_telp');
        $alamat = $request->input('alamat');
        $aktif = $request->input('aktif');
        
        
       
        $data = DB::table('user')
        ->where('id_user', $id_user)
        ->update(
            [
                "username"=> $username,
                "role" => $role,
                "nama" => $nama,
                "tgl_lahir"=> $tgl_lahir,
                "no_telp" => $no_telp,
                "alamat" => $alamat,
                "aktif" => $aktif
            ]
        );

        if ( $data ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Ubah Data User',
                'message_en' => 'Success Update Data User',
                'data' => $id_user,
                'nama' => $nama,
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Gagal Masukan Data User',
                'message_en' => 'Error Input Data User',
                'data' => $id_user,
                'nama' => $nama,
            ], 401);
        }
    }

    protected function jwt($data)
    {
        $key = "secret";
        $payload = [
            'iss' => "svc-jwtencoded",
            'id_user' => $data->id_user,
            'username' => $data->username,
            'username' => $data->username,
            'password' => $data->password,
            'iat' => time(),
            'exp' => time() + 60 * 60 * 2 * 365
        ];

        return JWT::encode($payload, $key);
    }

    public function login ( Request $request ) {
        $username = $request->input('username');
        $password = $request->input('password');
        $password = md5($password);

        $data = DB::table('user')
        ->select('*')
        ->where('username', $username)
        ->where('password', $password)
        ->get();

        $count = DB::table('user')
        ->select('*')
        ->where('username', $username)
        ->where('password', $password)
        ->count();


        // $input = array(
        //     'email' => "admin",
        //     "password" => "admin"
        // );

        if ( $count > 0 ) {
            return response()->json([
                'status' => 1,
                'message_ind' => 'Berhasil Login',
                'message_en' => 'Login Success',
                'data' => $data,
                // 'token' => $this->jwt($data),
            ], 200);
        } else {
            return response()->json([
                'status' => 0,
                'message_ind' => 'Password dan Username tidak sesuai',
                'message_en' => 'Password and Username not match',
            ], 401);
        }
    }

    
}
